class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery
  
  protected
  #returns the currently logged in user or nil if there isn't one
  def current_user
    return unless session[:user_id]
    @current_user ||= User.find_by_id(session[:user_id])
  end
  
  #make current_user available in templates as a helper
  helper_method :current_user
  
  #filter method to enforce a login requirement
  #applied as before_action to any controller we want to protect
  def authenticate
    logged_in? || access_denied
  end
  
  #predicate method to check for a logged in user
  def logged_in?
    current_user.is_a? User
  end
  
  #make logged_in? available in templates as a helper
  helper_method :logged_in?
  
  def access_denied
    redirect_to login_path, notice: "Please login to continue" and return false
  end
end
