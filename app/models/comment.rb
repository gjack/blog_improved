class Comment < ActiveRecord::Base
  validates_presence_of :body, :email, :name
  validate :article_should_be_published
  after_create :email_article_author
  after_create :send_comment_email
  belongs_to :article
  
  def article_should_be_published
    errors.add(:article_id, 'is not published yet') if article && !article.published?
  end
  
  def email_article_author
    puts "We will notify the author in chapter 9"
  end
  
  def send_comment_email
    Notifier.comment_added(self).deliver
  end
end
